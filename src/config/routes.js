import { Redirect } from 'react-router-dom';

import GetStarted from '../views/GetStarted/GetStarted';
import AppWrapper from '../views/AppWrapper/AppWrapper';
import Send from '../views/Send/Send';
import Sent from '../views/Sent/Sent';
import About from '../views/About/About';

const routes = [
  {
    path: "/start",
    component: GetStarted
  },
  {
    path: "/send",
    component: AppWrapper,
    routes: [
      {
        path: "/send/new",
        component: Send
      },
      {
        path: "/send/sent",
        component: Sent
      },
      {
        path: "/send/about",
        component: About
      },
      {
        component: Redirect,
        props: {
          to: "/send/new"
        }
      }
    ]
  },
  {
    component: Redirect,
    props: {
      to: "/start"
    }
  }
];
export default routes;