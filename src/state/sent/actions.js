import { PUSH_MAIL } from './actionTypes';

export const pushMail = payload => ({
  type: PUSH_MAIL,
  payload
});
