import createReducer from '../../utils/createReducer';
import { PUSH_MAIL } from './actionTypes';

export const sent = createReducer([], {
  [PUSH_MAIL]: (state, { payload }) => {
    let newArray = state.slice()
    newArray.push(payload)
    return newArray
  }
});
