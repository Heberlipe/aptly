import { combineReducers, createStore } from 'redux';
import { sent } from './sent/reducer';

const rootReducer = combineReducers({
  sent
});

export default createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);