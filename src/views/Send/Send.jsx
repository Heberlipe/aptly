import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import SuccessIcon from '@material-ui/icons/Done';
import ErrorIcon from '@material-ui/icons/HighlightOff';

import { red, green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core';

//Components
import SectionContainer from '../../components/SectionContainer/SectionContainer';
import SectionWrapper from '../../components/SectionWrapper/SectionWrapper';

// Config
import { getSendEmailSettings, NODE_URL } from '../../config';
import { pushMail } from '../../state/sent/actions';

const useStyles = makeStyles(theme => ({
  buttonContainer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 0,
    marginTop: theme.spacing(2)
  },
  button: {
    marginLeft: theme.spacing(2)
  },
  statusContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    paddingTop: theme.spacing(3)
  },
  status: {
    textAlign: 'center'
  },
  successIcon: {
    color: green['500']
  },
  errorIcon: {
    color: red['A700']
  }
}));

const inputList = [
  {
    name: 'to',
    inputProps: {
      autoComplete: "email",
      type: 'email'
    }
  },
  {
    name: 'from',
    inputProps: {
      autoComplete: "email",
      type: 'email'
    }
  },
  {
    name: 'subject',
    inputProps: {}
  },
  {
    name: 'message',
    inputProps: {
      multiline: true,
      rows: "4"
    }
  }
]

const initializeState = () => {
  let initialState = {
    hasError: {}
  };

  inputList.map(({ name }) => {
    initialState[name] = '';
    initialState.hasError[name] = false
    return 0;
  })

  return initialState;
}

const requestInitialState = {
  success: false,
  loading: false,
  error: false
}

const textFieldProps = {
  fullWidth: true,
  margin: "normal",
  variant: "outlined"
}

const Send = () => {
  const dispatch = useDispatch();
  const classes = useStyles();

  const [values, setValues] = useState(initializeState());
  const [requestState, setRequest] = useState(requestInitialState);


  const handleChange = name => event => setValues({ ...values, [name]: event.target.value });

  const handleReset = () => setValues(initializeState());

  const handleSubmit = e => {
    e.preventDefault();

    const hasError = {
      to: values.to === '',
      from: values.from === '',
      subject: values.subject === '',
      message: values.message === ''
    }

    setValues({ ...values, hasError });

    if (
      !hasError.to &&
      !hasError.from &&
      !hasError.subject &&
      !hasError.message
    ) {
      const mail = {
        to: values.to,
        from: values.from,
        subject: values.subject,
        value: values.message
      }

      setRequest({ ...requestInitialState, loading: true });

      fetch(NODE_URL, getSendEmailSettings(mail))
        .then(({ status }) => {
          if (status === 200) {
            setRequest({
              success: true,
              loading: false,
              error: false
            });
            handleReset();
            dispatch(pushMail(mail))
          } else {
            throw new Error();
          }
        })
        .catch(error => setRequest({
          success: false,
          loading: false,
          error
        }))
    }
  }

  return (
    <SectionWrapper>
      <SectionContainer>
        <Typography variant="h5">
          SEND NEW E-MAIL
        </Typography>

        <Container component="form" onSubmit={handleSubmit}>
          {inputList.map(({ name, inputProps }) => (
            <TextField
              {...textFieldProps}
              {...inputProps}
              disabled={requestState.loading}
              error={values.hasError[name]}
              key={`new-email-input-${name}`}
              label={name.toUpperCase()}
              name={name}
              onChange={handleChange(name)}
              value={values[name]}

            />
          ))}

          <Container className={classes.buttonContainer}>
            <Button
              className={classes.button}
              disabled={requestState.loading}
              onClick={handleReset}
              variant="contained"
            >
              Reset
            </Button>
            <Button
              className={classes.button}
              color="primary"
              disabled={requestState.loading}
              type='submit'
              variant="contained"
            >
              Send
            </Button>
          </Container>

          {requestState.loading &&
            <Container className={classes.statusContainer}>
              <CircularProgress />
            </Container>
          }

          {requestState.success &&
            <Container className={classes.statusContainer}>
              <Typography variant="h6">
                Success!
              </Typography>
              <Typography variant="body1">
                The message was sent
              </Typography>
              <SuccessIcon className={classes.successIcon} />
            </Container>
          }

          {requestState.error &&
            <Container className={classes.statusContainer}>
              <Typography variant="h6">
                Ups!
              </Typography>
              <Typography variant="body1">
                Something went wrong
              </Typography>
              <ErrorIcon className={classes.errorIcon} />
            </Container>
          }
        </Container>
      </SectionContainer>
    </SectionWrapper>
  )
};

export default Send;