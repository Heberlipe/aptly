import React from 'react';
import Button from '@material-ui/core/Button';
import { Link as RouterLink } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';

import SectionContainer from '../../components/SectionContainer/SectionContainer';

const Start = React.forwardRef((props, ref) => (
  <RouterLink innerRef={ref} to="/send" {...props} />
));

const GetStarted = () => {
  return (
    <SectionContainer fullHeight>
      <Typography variant="h2">
        Welcome!
      </Typography>
      <p>Welcome to the easy e-mail sender!</p>
      <Button component={Start} variant='contained' color='primary'>
        Start
      </Button>
    </SectionContainer>
  )
}

export default GetStarted;