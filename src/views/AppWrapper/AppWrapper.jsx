import React from 'react'
import PropTypes from 'prop-types';
import { Switch } from 'react-router-dom';
import NavBar from '../../components/NavBar/NavBar';
import buildRoutes from '../../utils/buildRoutes';

const AppWrapper = ({ routes }) => (
  <div>
    <NavBar />
    <Switch>
      {buildRoutes(routes)}
    </Switch>
  </div>
);

AppWrapper.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default AppWrapper;
