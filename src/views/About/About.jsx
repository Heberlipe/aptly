import React from 'react';

import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Typography from '@material-ui/core/Typography';

import SectionWrapper from '../../components/SectionWrapper/SectionWrapper';
import SectionContainer from '../../components/SectionContainer/SectionContainer';

const repoUrl = 'https://bitbucket.org/Heberlipe/aptly/src/master/';

const About = () => (
  <SectionWrapper>
    <SectionContainer>
      <Container>
        <Typography gutterBottom variant='h5'>Easy Email Sender</Typography>
        <Typography paragraph gutterBottom variant='body2'>
          This app is a Code Challenge. It sends emails via SendGrid API and stores it in a local temp history.
        </Typography>
        <Typography paragraph gutterBottom variant='body2'>
          Bitbucket repository can be found <Link href={repoUrl}>here</Link>.
        </Typography>
        <Typography gutterBottom variant='h6'>Libraries Used</Typography>
        <List>
          <ListItem>
            <Typography variant='body2'>
              React (Hooks)
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              React Router
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              Redux
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              Material UI
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              Styled Components
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              Firebase (hosting webpage)
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              Node JS (Express) for handling requests
            </Typography>
          </ListItem>
          <ListItem>
            <Typography variant='body2'>
              Heroku (hosting node server)
            </Typography>
          </ListItem>
        </List>
      </Container>
    </SectionContainer>
  </SectionWrapper>
)

export default About;