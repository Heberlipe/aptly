import React from 'react';
import { useSelector } from 'react-redux';

import SectionWrapper from '../../components/SectionWrapper/SectionWrapper';
import SectionContainer from '../../components/SectionContainer/SectionContainer';

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
  panel: {
    width: '100%'
  },
  detail: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  detailContainer: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    marginTop: theme.spacing(2)
  }
}));

const Sent = () => {
  const [expanded, setExpanded] = React.useState(false);
  const sent = useSelector(state => state.sent);
  const classes = useStyles();

  const handleChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <SectionWrapper>
      <SectionContainer>
        {
          sent.length < 1 &&
          <Container className={classes.detailContainer}>
            <Typography variant='h6'>No emails sent</Typography>
            <Typography className={classes.content}>
              Send a new email and it will appear here. Note that closing or refreshing the site deletes the sent history.
            </Typography>
          </Container>
        }
        {sent.map((mail, index) => (
          <ExpansionPanel className={classes.panel} key={index} expanded={index === expanded} onChange={handleChange(index)}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} >
              <Typography variant='h6'>{mail.subject}</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails className={classes.detailContainer}>
              <Typography className={classes.detail}>To: {mail.to}</Typography>
              <Typography className={classes.detail}>From: {mail.from}</Typography>
              <Typography className={classes.content}>{mail.value}</Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        ))}
      </SectionContainer>
    </SectionWrapper>
  )
};

export default Sent;