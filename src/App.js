import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';
import './App.css';

import store from './state/store';
import routes from './config/routes';
import buildRoutes from './utils/buildRoutes';

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        {buildRoutes(routes)}
      </Switch>
    </Router>
  </Provider>
);

export default App;
