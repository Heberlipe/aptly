import React from 'react';
import { Route } from 'react-router-dom';

const RouteWithSubRoutes = (route) => (
  <Route
    exact={route.isExact}
    path={route.path}
    render={props => (
      <route.component {...props} {...route.props} routes={route.routes} />
    )}
  />
);

const buildRoutes = routes => routes.map((route, i) => (
  <RouteWithSubRoutes key={i} {...route} />
));

export default buildRoutes;
