export const NODE_URL = 'https://aptly-node2.herokuapp.com/api/send';

export const getSendEmailSettings = (body) => ({
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(body)
})