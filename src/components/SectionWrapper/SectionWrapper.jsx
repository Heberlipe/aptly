import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from '../../config/styles';

const useStyles = makeStyles(theme => ({
  content: {
    [theme.breakpoints.up('sm')]: {
      paddingLeft: drawerWidth
    },
  },
  topSpace: theme.mixins.toolbar
}));

const SectionWrapper = ({ children }) => {
  const classes = useStyles();
  return (
    <main className={classes.content}>
      <div className={classes.topSpace} />
      {children}
    </main>
  )
}

SectionWrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};


export default SectionWrapper
