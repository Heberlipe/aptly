import styled from 'styled-components';

const SectionContainer = styled.section`
  align-items: center;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  height: ${props => props.fullHeight ? "100vh" : ""};
  justify-content: center;
  padding: 30px 10%;
`;

export default SectionContainer;